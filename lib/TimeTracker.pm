package TimeTracker;
use strict;
use autouse 'Data::Dumper' => 'Dumper';

use Tie::File::AsHash;
use File::stat;
use Date::Manip;
use LWP::Simple qw( getstore );

#define defaults, in case they aren't in the config file
use constant LOG        => '/var/log/time_tracker/time_tracker.log';
use constant STATEBIN   => '/usr/bin/state.sh';
use constant NOTIFYBIN  => '/usr/bin/notify-send';
use constant CONFIGFILE => '/etc/time_tracker.config';
use constant PIDFILE    => '/var/run/time_tracker.pid';
use constant STATUSFILE => '/tmp/time_tracker_status_MMDDYYY.txt';
use constant ICALFILE   => '/tmp/time_tracker_ical.ics';
use constant ICALAGE    => 3; #hours
use constant IDLELIMIT  => 15; #minutes

sub new {
	my ( $class, $args ) = @_;
	my $self = {};
	bless ( $self, $class );
	$self->init_config( { config => $args->{config} } );
	return $self;
}

sub log {
	my ( $self, $args ) = @_;

	return unless $self->{verbosity} >= 1 || $args->{err};

	my $time = scalar ( localtime ( time ) );

	open ( my $LOG, '>>', $self->{log} )
		|| die "failed: open log file $self->{log}\n";

	print $LOG "$time ===== BEGIN $args->{section} =====\n"
		if $args->{section};

	if ( $args->{string} ) {
		print $LOG $time;
		print $LOG " ERROR" if $args->{err};
		print $LOG " $args->{string}\n";
	}

	foreach ( @{$args->{array}} ) {
		print $LOG $time;
		print $LOG " ERROR" if $args->{err};
		print $LOG " $_\n";
	}

	print $LOG "$time ====== END $args->{section} ======\n"
		if $args->{section};

	close ( $LOG );
	return;
}

sub init_config {
	my ( $self, $args ) = @_;

	die "config is a required argument\n" unless $args->{config};

	tie my %config, 'Tie::File::AsHash', $args->{config}, split => '='
		|| die "failed: open config $args->{config}\n";

	foreach my $key ( keys %config ) {
		$self->{$key} = $config{$key};
	}

	untie %config;

   #force default config for anything that isn't specified
	$self->{log}        = LOG        unless $self->{log};
	$self->{stateBin}   = STATEBIN   unless $self->{stateBin};
	$self->{notifyBin}  = NOTIFYBIN  unless $self->{notifyBin};
	$self->{pidFile}    = PIDFILE    unless $self->{pidFile};
	$self->{statusFile} = STATUSFILE unless $self->{statusFile};
	$self->{icalFile}   = ICALFILE   unless $self->{icalFile};
	$self->{icalAge}    = ICALAGE    unless $self->{icalAge};
	$self->{idleLimit}  = IDLELIMIT  unless $self->{idleLimit};

	if ( $self->{verbosity} >= 2 ) {
		my @log;
		foreach ( keys %$self ) {
			push @log, "$_: $self->{$_}";
		}
		$self->log( { section => "CONFIG", array => \@log } );
	}
	return;
}

sub get_date {
	my ( $self, $offset ) = @_;

	my $time = ParseDateString( scalar( localtime( time ) ) );

	if ( $offset ) {
		$time = DateCalc( $time, "$offset days" );
	}

	return $self->get_time( { type => 'date', time => $time } );
}

sub get_status_file {
	my ( $self, $time ) = @_;

	my $replace;
	if( $time eq 'overtime' ) {
		$replace = 'overtime';
	} else {
		$replace = $self->get_time( { type => 'date', time => $time } );
	}

	my $status_file = $self->{statusFile};
		$status_file =~ s/MMDDYYYY/$replace/;

	return $status_file;
}

sub get_state {
	my ( $self, $args ) = @_;
	my @log;

	chomp( my $state = `$self->{stateBin}` );
	push( @log, "$self->{stateBin} has an issue: $?" ) if $?;
	push( @log, "$self->{stateBin} failed with error $@" ) if $@;
	$self->log( { err => 1, array => \@log } ) if $log[0];
	
	if ( $state ) {
		return 'inactive';
	} else {
		return 'working';
	}
}

sub get_previous_state {
	my ( $self ) = @_;
	my $status_file = $self->get_status_file();

	return unless -e $status_file;

	open ( STATUS, "tail -1 $status_file |" )	|| $self->log(
		{ err => 1, string => "failed to open status file $status_file" }
	) && return;
	chomp ( my @entry = <STATUS> );
	close ( STATUS );
	my ( $time, $state ) = split /\s-\s/, $entry[0];
	return $state;
}

sub change_state {
	my ( $self, $args ) = @_;

	die "previous, current required\n"
		unless $args->{previous} && $args->{current};

	my @log;
	push( @log, "was: $args->{previous}" );
	push( @log, "now: $args->{current}" );

	my $timestamp = scalar( localtime( time ) );

	if ( $args->{effective} ) {
		push( @log, "effective: $args->{effective}" );
		$timestamp = $args->{effective};
	}

	my $status_file = $self->get_status_file();
	open ( my $STATUS, ">>", $status_file )
		|| die "failed: open status file $status_file\n";

	print $STATUS "$timestamp - $args->{current}\n";
	close ( $STATUS );

	$self->log( { section => "STATE CHANGE", array => \@log } );

	return $args->{current};
}

sub get_calendar {
	my ( $self ) = @_;
	return unless $self->{icalURL};

	my @log = ( "pulling calendar: $self->{icalURL}" );
	my $err;

	my $response = getstore ( $self->{icalURL}, $self->{icalFile} );
	if ( $response != 200 ) {
		push( @log, "failed: ical pull ($response)" );
		$err = 1;
	}
	$self->log( { err => $err, section => "DOWNLOAD CALENDAR", array => \@log } );
	return;
}

sub stale_calendar {
	my ( $self, $args ) = @_;
	return 1 unless -e $args->{file};

	my $calendar_age = stat( $args->{file} )->mtime;
	my $now = $self->get_epoch( 'now' );

	if ( $now > $calendar_age + $args->{limit} ) {
		return 1;
	}
	return;
}

sub check_calendar {
	my ( $self, $time ) = @_;
	return unless -e $self->{icalFile};

	my ( @log, $err, $cal, $start, $end );

	my $date = $self->get_time( { type => 'ical', time => $time } );
	open( CAL, "grep $date $self->{icalFile} |" )
		|| $self->log( 
			{ err => 1, string => "failed to open $self->{icalFile}" }
		) && return 'inactive';

	$time = $self->get_time( { type => 'epoch', time => $time } );

	while( <CAL> ) {
		my( $dt, $timestamp ) = split /:/;
		if( $dt eq 'DTSTART' ) {
			$start = $self->get_epoch( $timestamp );
		} elsif( $dt eq 'DTEND' ) {
			$end = $self->get_epoch( $timestamp );
			if( $time >= $start && $time <= $end ) {
				close( CAL );
				return 'meeting';
			}
			$start = 0;
			$end   = 0;
		}
	}
	close( CAL );
	return 'inactive';
}

sub recheck_calendar {
	my( $self, $time ) = @_;

	my @records;
	my $changed = 0;

	my $status_file = $self->get_status_file( $time );
	open( my $FH, '<', $status_file )
		|| $self->log( { err => 1, string => "failed to open $status_file" } )
		&& return;
	chomp( my @status = <$FH> );
	close( $FH );

	foreach( @status ) {
		my( $timestamp, $state ) = split /\s-\s/;

# this logic is dumb - you need to rewrite it
		my $newstate = $self->check_calendar( $timestamp );
		if( $state eq 'inactive' && $newstate ne $state ) {
			$changed = 1;
			push( @records, "$timestamp - $newstate" );
		} else {
			push( @records, "$timestamp - $state" );
		}
	}

	return unless $changed;

	open( my $OUT, '>', $status_file ) || $self->log( {
		err => 1, string => "failed to open $status_file for writing"
	} ) && return;

	foreach( @records ) {
		print $OUT "$_\n";
	}

	close( $OUT );

	return;
}

sub get_time {
	my( $self, $args ) = @_;

	$args->{time} = 'now' unless $args->{time};

	if( $args->{time} =~ m{ ^\d{10,}$ }xms ) {
		$args->{time} = ParseDateString( scalar( localtime( $args->{time} ) ) );
	} else {
		$args->{time} = ParseDateString( $args->{time} );
	}

	if( $args->{type} eq 'manip' ) {
		return $args->{time};
	} elsif( $args->{type} eq 'epoch' ) {
		return UnixDate( $args->{time}, "%s" );
	} elsif( $args->{type} eq 'human' ) {
		return UnixDate( $args->{time}, "%c" );
	} elsif( $args->{type} eq 'ical' ) {
		return UnixDate( $args->{time}, "%Y%m%d" );
	} elsif( $args->{type} eq 'date' ) {
		return UnixDate( $args->{time}, "%m%d%Y" );
	} elsif( $args->{type} eq 'custom' && $args->{format} ) {
		return UnixDate( $args->{time}, $args->{format} );
	}
	return;
}

sub get_epoch {
	my ( $self, $timestamp ) = @_;
	return $self->get_time( { type => 'epoch', time => $timestamp } );
}

sub get_human {
	my ( $self, $timestamp ) = @_;
	return $self->get_time( { type => 'human', time => $timestamp } );
}

sub idle {
	my ( $self, $args ) = @_;

	my $timer;
	while ( $timer < $args->{limit} ) {
		my $new_state = $self->get_state();
		if ( $new_state ne $args->{state} ) {
			$self->log( { string => "ending idle and returning new state: $new_state" } )
				if $self->{verbosity} >= 3;
			return $new_state;
		} else {
			$self->log( { string => "state still $args->{state} - sleeping..." } )
				if $self->{verbosity} >= 3;
			$timer += 15;
			sleep 15;
		}
	}
	$self->log( { string => "idle limit reached. still $args->{state}" } )
		if $self->{verbosity} >= 3;
	return $args->{state};
}

sub calc_deltas {
	my ( $self, $file ) = @_;

	return unless -e $file;

	my @astatuses = ( 'working', 'meeting' );
	my @istatuses = ( 'inactive' );

	my ( $deltas, $previous, $current );
	open( my $FH, '<', $file ) || $self->log(
		{ err => 1, string => "failed to open $file" }
	) && return;
	chomp( my @time = <$FH> );
	close ( $FH );

	foreach ( @time ) {
		( $current->{timestamp}, $current->{status} ) = split /\s-\s/;
		if ( grep( /$current->{status}/, @astatuses ) ) {
			$current->{state} = 'active';
		} elsif( grep( /$current->{status}/, @istatuses ) ) {
			$current->{state} = 'inactive';
		} else {
			$self->log( { err => 1, string => "unknown status: $current->{status}" } );
		}

		if( $current->{state} ne $previous->{state} ) {
			my $delta = DateCalc(
				ParseDateString( $previous->{timestamp} ),
				ParseDateString( $current->{timestamp} )
			);

			if( $previous->{state} eq 'active' && $delta ) {
				push( @{$deltas->{active}}, $delta );
			} elsif ( $delta ) {
				push( @{$deltas->{inactive}}, $delta );
			}

			$previous->{state}     = $current->{state};
			$previous->{timestamp} = $current->{timestamp};
		}
	}
	return $deltas;
}

sub calc_overtime {
	my( $self, $deltas ) = @_;

	my $date = $self->get_time( { type => 'date', time => 'today' } );

	my $active_time = '0:0:0:0:0:0:0';

	foreach my $delta ( @{$deltas->{'active'}} ) {
		$active_time = DateCalc( $active_time, $delta );
	}

	my $overtime = DateCalc( $active_time, '-0:0:0:0:8:0:0' );

	return $overtime;
}

sub read_overtime {
	my( $self ) = @_;

	my $file = $self->get_status_file( 'overtime' );
	return unless -e $file;

	open( my $OT, '<', $file )
		|| $self->log( { err => 1, string => "failed to open $file" } ) && return;
	chomp( my @overtime = <$OT> );
	close( $OT );

	return \@overtime;
}

sub write_overtime {
	my( $self, $overtime ) = @_;

	my $date     = $self->get_time( { type => 'date', time => 'today' } );
	my $prevOT   = $self->read_overtime();
	my $file     = $self->get_status_file( 'overtime' );

	open( my $OT, '>', $file )
		|| $self->log( { err => 1, string => "failed to open $file for writing" } )
		&& return;

	my $found;
	foreach( @$prevOT ) {
		if( m{ ^$date }xms ) {
			print $OT "$date - $overtime\n";
			$found = 1;
		} else {
			print $OT "$_\n";
		}
	}

	print $OT "$date - $overtime\n" unless $found;

	close( $OT );
	return;
}

sub calc_departure {
	my ( $self, $deltas ) = @_;

	my ( $active_time, $inactive_time );

	my $inactive_time = '0:0:0:0:0:0:0';
	foreach my $delta ( @{$deltas->{'inactive'}} ) {
		$inactive_time = DateCalc( $inactive_time, $delta );
	}

	my $start = $self->get_start_time();
		$start = ParseDateString( $start );

	my $departure_time = DateCalc( $start, '0:0:0:0:8:0:0' );
		$departure_time = DateCalc( $departure_time, $inactive_time );

	return $departure_time;
}

sub get_start_time {
	my ( $self, $args ) = @_;

	my $file = $self->get_status_file();

	return 'now' unless -e $file;

	open( STATUS, "<", $file ) || $self->log(
		{ err => 1, string => "failed to open $file" }
	) && return 'now';

	chomp( my @status = <STATUS> );
	close( STATUS );

	foreach( @status ) {
		my( $time, $state ) = split /\s-\s/;
		return $time if $state eq 'working';
	}
	return 'now';
}

1;
