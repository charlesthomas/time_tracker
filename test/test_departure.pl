#!/usr/bin/perl
use strict;

use lib '/home/crthomas/git/charlesthomas/time_tracker/lib';
use TimeTracker;

my $time = "@ARGV";
   $time = 'now' unless $time;

my $conf = '/home/crthomas/.time_tracker/config';
my $tt = TimeTracker->new( { conf => $conf } );

my $status_file = $tt->get_status_file( $time );
my $deltas      = $tt->calc_deltas( $status_file );
my $departure   = $tt->calc_departure( $deltas );
my $human_time  = $tt->get_time( { type => 'human', time => $departure } );

print "departure time: $human_time\n";
exit;
