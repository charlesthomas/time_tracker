#!/usr/bin/perl
use strict;
use Data::Dumper;
use lib '/home/crthomas/git/charlesthomas/time_tracker/lib';
use TimeTracker;

my $tt = TimeTracker->new( {
	config => '/home/crthomas/git/charlesthomas/time_tracker/config/test',
} );

my @times = ( 7, 8, 9 );

foreach ( @times ) {
	my $delta = {
		active => [ "0:0:0:0:$_:0:0" ],
	};

	my $overtime = $tt->calc_overtime( $delta );

	print "time: $_\n";
	print "overtime: $overtime\n";
}

my $overtime = $tt->read_overtime();
print Dumper $overtime;

$tt->write_overtime( '0:0:0:0:0:5:0' );

$overtime = $tt->read_overtime();
print Dumper $overtime;

exit;
