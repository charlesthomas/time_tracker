#!/usr/bin/perl
use strict;
use warnings;

print "before\n";

my $time = "at the tone, the time will be\n" . scalar( localtime( time ) );

system( 'notify-send', 'Departure Time', "$time" );

print "after\n";
exit;
