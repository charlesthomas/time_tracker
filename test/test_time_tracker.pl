#!/usr/bin/perl
use strict;
use Date::Manip;
use File::stat;
use Getopt::Long;

use autouse 'Data::Dumper' => 'Dumper';

use lib "/home/$ENV{USER}/git/charlesthomas/time_tracker/lib";
use TimeTracker;

my( $all, $timer, $conf );
my( $bin, $get_state, $get_epoch, $get_date, $get_status_file );
my( $get_calendar, $check_calendar, $stale_calendar );
my( $get_previous_state, $change_state, $idle, $calc_deltas, $calc_departure );

my $args = GetOptions (
	'conf=s'             => \$conf,
	'timer=i'            => \$timer,
	'all'                => \$all,
	'bin'                => \$bin,
	'get-state'          => \$get_state,
	'get-epoch'          => \$get_epoch,
	'get-date'           => \$get_date,
	'get-status-file'    => \$get_status_file,
	'get-calendar'       => \$get_calendar,
	'check-calendar'     => \$check_calendar,
	'stale-calendar'     => \$stale_calendar,
	'get-previous-state' => \$get_previous_state,
	'change-state'       => \$change_state,
	'idle'               => \$idle,
	'calc-deltas'        => \$calc_deltas,
	'calc-departure'     => \$calc_departure,
);

die "conf file required argument" unless $conf && -e $conf;
$timer = 10 unless $timer;

my $tt = TimeTracker -> new ( { config => $conf } );

my @log;
push( @log, 'testing TimeTracker.pm' );

################################################################ CONFIRM BIN ###
if( $bin || $all ) {
	push( @log, '-----------------------------------' );
	push( @log, 'confirming state bin is in place...' );
	my $path = $tt->{stateBin};
	push( @log, "state bin should be here: $path and should be executable" );
	if ( -e $path ) {
		push( @log, 'state bin is where it should be' );
		if ( -x $path ) {
			push( @log, 'state bin is executable' );
			push( @log, 'RESULT: state bin PASSED' );
		} else {
			push( @log, 'state bin is not executable' );
			push( @log, 'RESULT: state bin FAILED' );
		}
	} else {
		push( @log, "state bin does not exist in $path" );
		push( @log, 'RESULT: state bin FAILED' );
	}
}
############################################################ END CONFIRM BIN ###

############################################################# TEST GET STATE ###
if( $get_state || $all ) {
	push( @log, '-----------------------------------' );
	$tt->log( { string => '!!!!!testing get_state - this requires action!!!!!' } );
	my $first_state = $tt->get_state();
	$tt->log( { string => "current state is $first_state" } );
	$tt->log( { string => "sleeping for $timer seconds and then checking state again" } );
	$tt->log( { string => 'lock your screen NOW!' } );
	for ( my $i = $timer; $i >= 0; $i-- ) {
		$tt->log( { string => "checking state in $i..." } );
		sleep 1;
	}
	my $second_state = $tt->get_state();

	push( @log, "testing get_state..." );
	push( @log, "first get_state test was '$first_state'" );
	push( @log, "second get_state test was '$second_state'" );
	push( @log, 'up to you to determine success or failure, depending on whether' );
	push( @log, 'you locked your screen in time for the second test' );

	while ( $tt->get_state() eq 'inactive' ) {
		$tt->log( { string => "waiting to be active again to continue tests..." } );
		sleep 1;
	}
}
######################################################### END TEST GET STATE ###

############################################################ TEST GET EPOCH ####
if( $get_epoch || $all ) {
	push( @log, '-----------------------------------' );
	push( @log, 'testing get_epoch...' );
	my $time = 'May 28th 2012 2:55:00 PM';
	my $epoch = $tt->get_epoch( $time );
	my $pass_epoch = 1338231300;
	push( @log, "time: $time", "expected return value: $pass_epoch" );
	push( @log, "actual return value: $epoch" );
	if ( $epoch == $pass_epoch ) {
		push( @log, 'RESULT: get_epoch test PASSED' );
	} else {
		push( @log, 'RESULT: get_epoch test FAILED!' );
	}
}
######################################################## END TEST GET EPOCH ####

############################################################## TEST GET DATE ###
if( $get_date || $all ) {
	push( @log, '-----------------------------------' );
	push( @log, 'testing get_date...' );
	my $get_date_fail;
	my $today = ParseDateString( scalar( localtime( time ) ) );
	my $two_weeks_ago = DateCalc( $today, "-14 days" );
	my $one_week_from_now = DateCalc( $today, "7 days" );

	my $date1_passed = UnixDate( $today,             "%m%d%Y" );
	my $date2_passed = UnixDate( $two_weeks_ago,     "%m%d%Y" );
	my $date3_passed = UnixDate( $one_week_from_now, "%m%d%Y" );

	my $date1_result = $tt->get_date();
	my $date2_result = $tt->get_date( -14 );
	my $date3_result = $tt->get_date( 7 );

	push( @log, 'date test 1:', "today is $date1_passed" );
	push( @log, "get_date returned: $date1_result" );
	if ( $date1_passed eq $date1_result ) {
		push( @log, 'date test 1 PASSED' );
	} else {
		$get_date_fail = 1;
		push( @log, 'date test 1 FAILED' );
	}

	push( @log, 'date test 2:', "two weeks ago is $date2_passed" );
	push( @log, "get_date returned: $date2_result" );
	if ( $date2_passed eq $date2_result ) {
		push( @log, 'date test 2 PASSED' );
	} else {
		$get_date_fail = 1;
		push( @log, 'date test 2 FAILED' );
	}

	push( @log, 'date test 3:', "one week from now is $date3_passed" );
	push( @log, "get_date returned: $date3_result" );
	if ( $date3_passed eq $date3_result ) {
		push( @log, 'date test 3 PASSED' );
	} else {
		$get_date_fail = 1;
		push( @log, 'date test 3 FAILED' );
	}

	if( $get_date_fail ) {
		push( @log, 'RESULT: get_date FAILED' );
	} else {
		push( @log, 'RESULT: get_date PASSED' );
	}
}
########################################################## END TEST GET DATE ###

####################################################### TEST GET STATUS FILE ###
if( $get_status_file || $all ) {
	push( @log, '-----------------------------------' );
	push( @log, 'testing get_status_file...' );
	my $pattern = $tt->{statusFile};
	my ( $status_file_warning, $status_file_fail );
	unless ( $pattern =~ m{ MMDDYYYY }xms ) {
		push( @log, 'your status file pattern does not contain MMDDYYYY' );
		push( @log, 'this means you can only track one day at a time' );
		$status_file_warning = 1;
	}

	my $today_file_passed             = $pattern;
	my $two_weeks_ago_file_passed     = $pattern;
	my $one_week_from_now_file_passed = $pattern;
	my $overtime_passed               = $pattern;

	my $today = ParseDateString( scalar( localtime( time ) ) );
	my $two_weeks_ago = DateCalc( $today, "-14 days" );
	my $one_week_from_now = DateCalc( $today, "7 days" );

	my $date1_passed    = UnixDate( $today,             "%m%d%Y" );
	my $date2_passed    = UnixDate( $two_weeks_ago,     "%m%d%Y" );
	my $date3_passed    = UnixDate( $one_week_from_now, "%m%d%Y" );

	$today_file_passed             =~ s/MMDDYYYY/$date1_passed/;
	$two_weeks_ago_file_passed     =~ s/MMDDYYYY/$date2_passed/;
	$one_week_from_now_file_passed =~ s/MMDDYYYY/$date3_passed/;
	$overtime_passed               =~ s/MMDDYYYY/overtime/;

	my $today_file_result             = $tt->get_status_file();
	my $two_weeks_ago_file_result     = $tt->get_status_file( 'two weeks ago' );
	my $one_week_from_now_file_result = $tt->get_status_file( '7 days from now' );
	my $overtime_result               = $tt->get_status_file( 'overtime' );

	push( @log, 'get_status_file test 1:' );
	push( @log, "today's status file is $today_file_passed" );
	push( @log, "get_status_file returned $today_file_result" );
	if( $today_file_result eq $today_file_passed ) {
		push( @log, 'get_status_file test 1 PASSED' );
	} else { 
		$status_file_fail = 1;
		push( @log, 'get_status_file test 1 FAILED' );
	}

	push( @log, 'get_status_file test 2:' );
	push( @log, "two weeks ago's status file is $two_weeks_ago_file_passed" );
	push( @log, "get_status_file returned $two_weeks_ago_file_result" );
	if( $two_weeks_ago_file_result eq $two_weeks_ago_file_passed ) {
		push( @log, 'get_status_file test 2 PASSED' );
	} else { 
		$status_file_fail = 1;
		push( @log, 'get_status_file test 2 FAILED' );
	}

	push( @log, 'get_status_file test 3:' );
	push( @log, "one week from now's status file is $one_week_from_now_file_passed" );
	push( @log, "get_status_file returned $one_week_from_now_file_result" );
	if( $one_week_from_now_file_result eq $one_week_from_now_file_passed ) {
		push( @log, 'get_status_file test 3 PASSED' );
	} else { 
		$status_file_fail = 1;
		push( @log, 'get_status_file test 3 FAILED' );
	}

	push( @log, 'get_status_file test 4:' );
	push( @log, "overtime status file is $overtime_passed" );
	push( @log, "get_status_file returned $overtime_result" );
	if( $overtime_result eq $overtime_passed ) {
		push( @log, 'get_status_file test 4 PASSED' );
	} else {
		$status_file_fail = 1;
		push( @log, 'get_status_file test 4 FAILED' );
	}

	if( $status_file_fail ) {
		push( @log, 'RESULT: get_status_file FAILED' );
	} elsif( $status_file_warning ) {
		push( @log, 'RESULT: get_status_file PASSED WITH WARNINGS' );
	} else {
		push( @log, 'RESULT: get_status_file PASSED' );
	}
}
################################################### END TEST GET STATUS FILE ###

########################################################## TEST GET CALENDAR ###
if( $get_calendar || $all ) {
	push( @log, '-----------------------------------' );
	push( @log, 'testing get_calendar...' );
	my $get_ical_fail;
	if( ! $tt->{icalURL} ) {
		push( @log, 'no url defined. skipping test' );
	} else {
		my $icalFileAge;
		if ( -e $tt->{icalFile} ) {
			push( @log, "ical file already exists in $tt->{icalFile}" );
			$icalFileAge = stat( $tt->{icalFile} )->mtime;
		}
		$tt->get_calendar();
		if ( -e $tt->{icalFile} ) {
			if ( $icalFileAge ) {
				my $newIcalFileAge = stat( $tt->{icalFile} )->mtime;
				if ( $icalFileAge < $newIcalFileAge ) {
					push( @log, "$tt->{icalFile} exists and newer than before test" );
				} else {
					push( @log, "$tt->{icalFile} is not newer than before test" );
					$get_ical_fail = 1;
				}
			} else {
				push( @log, "$tt->{icalFile} exists now and didn't before" );
			}
		}
		if ( $get_ical_fail ) {
			push( @log, 'RESULT: get_calendar FAILED' );
		} else {
			push( @log, 'RESULT: get_calendar PASSED' );
		}
	}
}
###################################################### END TEST GET CALENDAR ###

######################################################## TEST CHECK CALENDAR ###
if( $check_calendar || $all ) {
	push( @log, '-----------------------------------' );
	push( @log, 'testing check_calendar...' );
	if ( ! -e $tt->{icalFile} ) {
		push( @log, "$tt->{icalFile} doesn't exist. skipping test" );
	} else {
		my $check_calendar_fail;
		my $no_meeting_human = 'may 28th 2012 5:10 PM';
		my $meeting_human    = 'may 28th 2012 2:35 PM';
		my $no_meeting_epoch = $tt->get_epoch( $no_meeting_human );
		my $meeting_epoch    = $tt->get_epoch( $meeting_human    );

		my $no_meeting_result = $tt->check_calendar( $no_meeting_epoch );
		my $meeting_result    = $tt->check_calendar( $meeting_epoch    );

		push( @log, 'test 1: no meeting' );
		push( @log, 'expected: inactive' );
		push( @log, "actual: $no_meeting_result" );
		if ( $no_meeting_result eq 'inactive' ) {
			push( @log, 'check_calendar test 1 PASSED' );
		} else {
			$check_calendar_fail = 1;
			push( @log, 'check_calendar test 1 FAILED' );
		}

		push( @log, 'test 2: meeting' );
		push( @log, 'expected: meeting' );
		push( @log, "actual: $meeting_result" );
		if ( $meeting_result eq 'meeting' ) {
			push( @log, 'check_calendar test 2 PASSED' );
		} else {
			$check_calendar_fail = 1;
			push( @log, 'check_calendar test 2 FAILED' );
		}

		if ( $check_calendar_fail ) {
			push( @log, 'RESULT: check_calendar FAILED' );
		} else {
			push( @log, 'RESULT: check_calendar PASSED' );
		}
	}
}
#################################################### END TEST CHECK CALENDAR ###

#################################################### TEST GET PREVIOUS STATE ###
if( $get_previous_state || $all ) {
	push( @log, '-----------------------------------' );
	push( @log, 'testing get_previous_state...' );
	my $previous_state_fail;
	my $previous_state_file = $tt->get_status_file();
	if ( -e $previous_state_file ) {
		$tt->log( { string => "!!!!!!$previous_state_file exists but can't for testing!!!!!" } );
		$tt->log( { string => '!!!!!will delete file, unless you kill the test first!!!!!' } );
		for ( my $i = $timer; $i >= 0; $i-- ) {
			$tt->log( { string => "deleting $previous_state_file in $i..." } );
			sleep 1;
		}
		unlink $previous_state_file;
	}
	push( @log, 'test 1: no previous state' );
	my $previous_state = $tt->get_previous_state();
	push( @log, 'expected: <NULL>' );
	push( @log, "actual: $previous_state" );
	if ( ! $previous_state ) {
		push( @log, 'test 1 PASSED' );
	} else {
		$previous_state_fail = 1;
		push( @log, 'test 1 FAILED' );
	}

	my $previous_state_passed = 'previous status file test status';
	push( @log, 'test 2: pull previous state from file' );
	open ( PREV, ">$previous_state_file" )
		|| die "failed to open $previous_state_file for writing\n";
	print PREV scalar( localtime( time ) ) . " - $previous_state_passed\n";
	close ( PREV );
	$previous_state = $tt->get_previous_state();
	push( @log, "expected: $previous_state_passed" );
	push( @log, "actual: $previous_state" );
	if ( $previous_state eq $previous_state_passed ) {
		push( @log, 'test 2 PASSED' );
	} else {
		$previous_state_fail = 1;
		push( @log, 'test 2 FAILED' );
	}

	if ( $previous_state_fail ) {
		push( @log, 'RESULT: get_previous_state FAILED' );
	} else {
		push( @log, 'RESULT: get_previous_state PASSED' );
	}
}
################################################ END TEST GET PREVIOUS STATE ###

########################################################## TEST CHANGE STATE ###
if( $change_state || $all ) {
	push( @log, '-----------------------------------' );
	push( @log, 'testing change_state...' );
	my $change_state_fail;

	push( @log, 'test 1: from working to inactive' );
	my $change_state_test1_passed = 'inactive';
	push( @log, "expected: $change_state_test1_passed" );
	my $change_state_test1_result = $tt->change_state (
		{ previous => 'working', current => $change_state_test1_passed }
	);
	push( @log, "actual: $change_state_test1_result" );
	if ( $change_state_test1_result eq $change_state_test1_passed ) {
		push( @log, 'test 1 PASSED' );
	} else {
		$change_state_fail = 1;
		push( @log, 'test 1 FAILED' );
	}

	push( @log, 'test 2: from inactive to working' );
	my $change_state_test2_passed = 'working';
	push( @log, "expected: $change_state_test2_passed" );
	my $change_state_test2_result = $tt->change_state (
		{ previous => 'inactive', current => $change_state_test2_passed }
	);
	push( @log, "actual: $change_state_test2_result" );
	if ( $change_state_test2_result eq $change_state_test2_passed ) {
		push( @log, 'test 2 PASSED' );
	} else {
		$change_state_fail = 1;
		push( @log, 'test 2 FAILED' );
	}

	if ( $change_state_fail ) {
		push( @log, 'RESULT: change_state FAILED' );
	} else {
		push( @log, 'RESULT: change_state PASSED' );
	}
}
###################################################### END TEST CHANGE STATE ###

################################################################## TEST IDLE ###
if( $idle || $all ) {
	push( @log, '-----------------------------------' );
	push( @log, 'testing idle...' );
	my $idle_fail;
	my $idle_test1_result = $tt->idle( { limit => 15, state => 'inactive' } );
	if ( $idle_test1_result eq 'working' ) {
		push( @log, 'test 1 PASSED' );
	} else {
		$idle_fail = 1;
		push( @log, 'test 1 FAILED' );
	}
	$tt->log( { string => 'this test should take a bit if it works as expected.' } );

	my $idle_test2_result = $tt->idle( { limit => 15, state => 'working' } );
	if ( $idle_test2_result eq 'working' ) {
		push( @log, 'test 2 PASSED' );
	} else {
		$idle_fail = 1;
		push( @log, 'test 2 FAILED' );
	}

	if ( $idle_fail ) {
		push( @log, 'RESULT: idle FAILED' );
	} else {
		push( @log, 'RESULT: idle PASSED' );
	}
}
############################################################## END TEST IDLE ###

######################################################## TEST STALE CALENDAR ###
if( $stale_calendar || $all ) {
	push( @log, '-----------------------------------' );
	push( @log, 'testing stale_calendar' );
	my $stale_fail;
	my $stale_file = '/tmp/test_time_tracker_stale_file';
	if ( -e $stale_file ) {
		$tt->log( { string => "!!!!!$stale_file exists!!!!!" } );
		$tt->log( { string => '!!!!!deleting unless you kill me!!!!!' } );
		for ( my $i = $timer; $i >= 0; $i-- ) {
			$tt->log( { string => "deleting in $i..." } );
			sleep 1;
		}
		unlink $stale_file;
	}
	my $stale_test1 = $tt->stale_calendar( { file => $stale_file, limit => 1 } );
	push( @log, 'test 1: file does not exist' );
	push( @log, 'expected: 1' );
	push( @log, "actual: $stale_test1" );
	if ( $stale_test1 ) {
		push( @log, 'test 1 PASSED' );
	} else {
		$stale_fail = 1;
		push( @log, 'test 1 FAILED' );
	}

	`touch $stale_file`;
	my $stale_test2 = $tt->stale_calendar( { file => $stale_file, limit => 5 } );
	push( @log, 'test 2: file is not stale' );
	push( @log, 'expected: <NULL>' );
	push( @log, "actual: $stale_test2" );
	if ( ! $stale_test2 ) {
		push( @log, 'test 2 PASSED' );
	} else {
		$stale_fail = 1;
		push( @log, 'test 2 FAILED' );
	}

	$tt->log( { string => 'sleeping 5 seconds for stale file test 3...' } );
	sleep 5;
	my $stale_test3 = $tt->stale_calendar( { file => $stale_file, limit => 3 } );
	push( @log, 'test 3: file is stale' );
	push( @log, 'expected: 1' );
	push( @log, "actual: $stale_test3" );
	if ( $stale_test3 ) {
		push( @log, 'test 3 PASSED' );
	} else {
		$stale_fail = 1;
		push( @log, 'test 3 FAILED' );
	}

	if ( $stale_fail ) {
		push( @log, 'RESULT: stale_calendar FAILED' );
	} else {
		push( @log, 'RESULT: stale_calendar PASSED' );
	}
}
#################################################### END TEST STALE CALENDAR ###

########################################################### TEST CALC DELTAS ###
if( $calc_deltas || $all ) {
	push( @log, '-----------------------------------' );
	push( @log, 'testing calc_deltas' );
	my $calc_delta_fail;
	my $file = $tt->get_status_file();
	open ( my $FH, '>', $file )
		|| die "failed to open $file for testing calc_deltas";

	print $FH "Tue May 29 08:30:00 2012 - working\n";
	print $FH "Tue May 29 09:00:00 2012 - meeting\n";
	print $FH "Tue May 29 10:00:00 2012 - working\n";
	print $FH "Tue May 29 12:00:00 2012 - inactive\n";
	print $FH "Tue May 29 13:00:00 2012 - working\n";
	print $FH "Tue May 29 14:00:00 2012 - meeting\n";
	print $FH "Tue May 29 15:00:00 2012 - working\n";
	print $FH "Tue May 29 15:30:00 2012 - inactive\n";
	print $FH "Tue May 29 16:00:00 2012 - working\n";
	print $FH "Tue May 29 18:00:00 2012 - inactive\n";
	close ( $FH );

	my $calc_delta_result = $tt->calc_deltas( $file );
	my $calc_delta_passed = {
		inactive => [
			'0:0:0:0:1:0:0',
			'0:0:0:0:0:30:0',
		],
		active => [
			'0:0:0:0:3:30:0',
			'0:0:0:0:2:30:0',
			'0:0:0:0:2:0:0',
		],
	};

	for ( my $i = 1; $i < 4; $i++ ) {
		push( @log, "active test $i:" );
		my $expected = @{$calc_delta_passed->{active}}[$i-1];
		my $actual   = @{$calc_delta_result->{active}}[$i-1];
		push( @log, "expected: $expected" );
		push( @log, "actual: $actual" );
		if( $actual eq $expected ) {
			push( @log, "active test $i PASSED" );
		} else {
			$calc_delta_fail = 1;
			push( @log, "active test $i FAILED" );
		}
	}

	for ( my $i = 1; $i < 3; $i++ ) {
		push( @log, "inactive test $i:" );
		my $expected = @{$calc_delta_passed->{inactive}}[$i-1];
		my $actual   = @{$calc_delta_result->{inactive}}[$i-1];
		push( @log, "expected: $expected" );
		push( @log, "actual: $actual" );
		if( $actual eq $expected ) {
			push( @log, "inactive test $i PASSED" );
		} else {
			$calc_delta_fail = 1;
			push( @log, "inactive test $i FAILED" );
		}
	}

	if ( $calc_delta_fail ) {
		push( @log, 'RESULT: calc_deltas FAILED' );
	} else {
		push( @log, 'RESULT: calc_deltas PASSED' );
	}
}
####################################################### END TEST CALC DELTAS ###

######################################################## TEST CALC DEPARTURE ###
if( $calc_departure || $all ) {
	push( @log, '-----------------------------------' );
	push( @log, 'testing calc_departure...' );
	my $calc_departure_fail;

	my $file = $tt->get_status_file();
	my $time = $tt->get_human( '8 AM Today' );
	open( START, ">", $file )
		|| die "opened $file failed while testing calc_departure\n";
	print START "$time - working\n";
	close( START );

	my $test1_deltas = {
		'inactive' => [
			'0:0:0:0:0:5:0',
			'0:0:0:0:0:20:0',
			'0:0:0:0:0:35:0',
		],
	};

	my $test1_passed = $tt->get_human( '5 PM Today' );
	my $test1_result = UnixDate( $tt->calc_departure( $test1_deltas ), "%c" );

	push( @log, 'test 1:' );
	push( @log, "expected: $test1_passed" );
	push( @log, "actual: $test1_result" );
	if( $test1_result eq $test1_passed ) {
		push( @log, 'test 1 PASSED' );
	} else {
		$calc_departure_fail = 1;
		push( @log, 'test 1 FAILED' );
	}

	my $test2_deltas = {
		'inactive' => [
			'0:0:0:0:0:5:0',
			'0:0:0:0:0:20:0',
			'0:0:0:0:0:35:0',
			'0:0:0:0:1:17:0',
		],
	};

	my $test2_passed = $tt->get_human( '6:17 PM Today' );
	my $test2_result = UnixDate( $tt->calc_departure( $test2_deltas ), "%c" );

	push( @log, 'test 2:' );
	push( @log, "expected: $test2_passed" );
	push( @log, "actual: $test2_result" );
	if( $test2_result eq $test2_passed ) {
		push( @log, 'test 2 PASSED' );
	} else {
		$calc_departure_fail = 1;
		push( @log, 'test 2 FAILED' );
	}

	my $test3_passed = $tt->get_human( '4 PM Today' );
	my $test3_result = UnixDate( $tt->calc_departure(), "%c" );

	push( @log, 'test 3:' );
	push( @log, "expected: $test3_passed" );
	push( @log, "actual: $test3_result" );
	if( $test3_result eq $test3_passed ) {
		push( @log, 'test 3 PASSED' );
	} else {
		$calc_departure_fail = 1;
		push( @log, 'test 3 FAILED' );
	}

	if ( $calc_departure_fail ) {
		push( @log, 'RESULT: calc_departure FAILED' );
	} else {
		push( @log, 'RESULT: calc_departure PASSED' );
	}
}
#################################################### END TEST CALC DEPARTURE ###
push( @log, '-----------------------------------' );

#force verbosity for logging results
$tt->{verbosity} = 2 unless $tt->{verbosity} >= 2;
$tt->log( { section => 'TEST RESULTS', array => \@log } );
exit;
