#!/usr/bin/perl
use strict;
use Data::Dumper;
use lib '/home/crthomas/git/charlesthomas/time_tracker/lib';
use TimeTracker;

my $tt = TimeTracker->new( {
	config => '/home/crthomas/.time_tracker/config',
} );

$tt->recheck_calendar( 'today' );

exit;
