#!/usr/bin/perl
use strict;
use autouse 'Data::Dumper' => 'Dumper';
use lib '/home/crthomas/git/charlesthomas/time_tracker/lib';
use TimeTracker;

my $config  = '/home/crthomas/git/charlesthomas/time_tracker';
   $config .= '/config/ical_test_config';

my $tt = TimeTracker->new( { conf => $config } );

$tt->get_calendar();

#my $epoch = $tt->get_epoch( 'June 18th 2PM' );
my $epoch = $tt->get_time( { type => 'epoch', time => 'now' } );

my $result = $tt->check_calendar( $epoch );
print "result: $result\n";
exit;
