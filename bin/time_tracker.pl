#!/usr/bin/perl
use strict;

use TimeTracker;
use Proc::Daemon;
use Getopt::Long;
use autouse 'Data::Dumper' => 'Dumper';

my $testing;
my $config = '/etc/time_tracker.config';
my $sleep  = 15;

my $args = GetOptions (
	'testing'  => \$testing,
	'config=s' => \$config,
	'sleep=i'  => \$sleep,
);

my $tt = TimeTracker->new( { config => $config } );

Proc::Daemon::Init( {
	pid_file => $tt->{pidFile},
} )  unless $testing;

$tt->log( { section => 'STARTUP', string => "starting $0" } );

my $run = 1;
$SIG{USR1} = \&_reinit;
$SIG{USR2} = \&_calendar;
$SIG{ALRM} = \&_departure;
$SIG{TERM} = sub { $run = 0; };
$SIG{INT}  = sub { $run = 0; } if $testing;

my ( $state, $prevState );

while ( $run ) {

	if ( $testing ) {
		$testing = 0;
		_departure();
	}

	unless ( $prevState ) {
		my @log = ( "no previous state. getting from status file." );
		$prevState = $tt->get_previous_state();

		push( @log, "previous state: $prevState" );
		$tt->log( { section => "PREVIOUS STATE", array => \@log } );
	}

	$state = $tt->get_state();
	$tt->log( { string => "current state: $state" } ) if $tt->{verbosity} >= 3;

	if ( $state ne $prevState ) {
		my $effective;
			$effective = scalar( localtime( time ) ) if $state eq 'inactive';
		my $now = $tt->get_epoch( 'now' );

		if ( $state eq 'inactive' ) {
			$state = $tt->idle(
				{ state => 'inactive', limit => $tt->{idleLimit} * 60 }
			);
		}
		if ( $state eq 'inactive' && $tt->{icalURL} ) {
			$tt->get_calendar()
				if $tt->stale_calendar( { file => $tt->{icalFile}, limit => $tt->{icalAge} * 60 * 60 } );
			$state = $tt->check_calendar( $now );
		}
		if ( $state ne $prevState ) {
			my $args = {
				previous => $prevState,
				current  => $state,
			};
			$args->{previous}  = 'null' unless $args->{previous};
			$args->{effective} = $effective if $effective;
			$prevState = $tt->change_state( $args );

			if( $prevState eq 'working' ) {
				_departure();
			} elsif( $prevState eq 'inactive' ) {
				_overtime();
			}
		}
	}
	sleep $sleep if $run;
}

$tt->log( { string => "exiting" } );
exit;

sub _reinit {
	undef $tt;
	$tt = TimeTracker->new( { config => $config } );
	return;
}

sub _departure {
	my $status_file = $tt->get_status_file( 'now' );
	my $deltas = $tt->calc_deltas( $status_file );
	my $departure = $tt->calc_departure( $deltas );
	my $time = $tt->get_time( { type => 'human', time => $departure } );

	my @cmd = (
		'/usr/bin/notify-send',
		'-i',
		'/home/crthomas/Dropbox/Photos/icons/stopwatch.png',
		'Departure Time',
		$time,
	);
	system( @cmd );
	return;
}

sub _overtime {
	my $status_file = $tt->get_status_file( 'now' );
	my $deltas = $tt->calc_deltas( $status_file );
	my $overtime = $tt->calc_overtime( $deltas );

	$tt->write_overtime( $overtime );
}

sub _calendar {
	return unless $tt->{icalURL};

	$tt->get_calendar();
	$tt->recheck_calendar( 'today' );
}
