#!/bin/bash
DISPLAY=:0.0

if [ -z "`/usr/bin/gnome-screensaver-command --query | grep "is active"`" ]; then
	echo 0
else
	echo 1
fi
