#!/bin/bash

#perl requirements
sudo cpan -i Tie::File::AsHash Date::Manip Proc::Daemon

#log dir
sudo mkdir -p /var/log/time_tracker/

#bin
cp -vi bin/time_tracker.pl /usr/bin/
cp -vi bin/state.sh /usr/bin/

#config
cp -vi config/install_config /etc/time_tracker.config

#lib
cp -vi lib/TimeTracker.pm /usr/lib/perl5/

#init.d
cp -vi init.d/time_tracker /etc/init.d
update-rc.d time_tracker defaults
